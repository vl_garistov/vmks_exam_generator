/*
 *	VMKS exam generator - A simple program for pseudo-randomly generating different variants of an embedded programming exam
 *	Copyright (C) 2022 - 2023 Vladimir Garistov <vl.garistov@gmail.com>
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as published
 *	by the Free Software Foundation, version 3.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::process;
use std::ffi::OsString;

use clap::{Arg, ArgAction, Command};
use clap::error::ErrorKind;
use vmks_exam_generator::Config;

fn main()
{
	// Declare command-line arguments
	let cli = Command::new("VMKS Exam Generator")
		.version("1.3.1")
		.author("Vladimir Garistov <vl.garistov@gmail.com>")
		.about("A simple CLI program for pseudo-randomly generating different variants of an embedded programming exam")
		.long_about(
			"A simple CLI program for pseudo-randomly generating different variants of an embedded programming exam.\n\
			 Designed for Technological school \"Electronic Systems\" in Sofia, Bulgaria.\n\n\
			 When a new variant is generated an adjustable subset of questions is picked at random from a question bank.\n\
			 Both questions and possible answers for multiple choice questions are shuffled.\n\
			 Supports randomly picking parts of the question text from a predefined list of possible values.\n\
			 This makes it possible to generate slightly different tasks such as\n\
			 \"Write a program that prints asterisks in the shape of a square\"\n\
			 and\n\
			 \"Write a program that prints hashtags in the shape of a triangle\"."
		)
		.arg(
			Arg::new("variants")
				.short('n')
				.long("variants")
				.value_name("VARIANTS")
				.value_parser(clap::value_parser!(u32))
				.default_value("30")
				.help("Number of variants to generate")
				.required(false)
		)
		.arg(
			Arg::new("question-bank")
				.short('q')
				.long("question-bank")
				.value_name("FILENAME")
				.value_parser(clap::value_parser!(OsString))
				.default_value("questions.xml")
				.help("XML file specifying the questions")
				.required(false)
		)
		.arg(
			Arg::new("num-seed")
				.short('s')
				.long("num-seed")
				.value_name("SEED")
				.value_parser(clap::value_parser!(u64))
				.help("Use the number SEED to seed the random number generator")
				.required(false)
		)
		.arg(
			Arg::new("text-seed")
				.short('S')
				.long("text-seed")
				.value_name("SEED")
				.help("Use the hash-sum of the string SEED to seed the random number generator")
				.required(false)
				.conflicts_with("num-seed")
		)
		.arg(
			Arg::new("plaintext")
				.short('t')
				.long("plain-text")
				.default_value("false")
				.help("Output plaintext instead of pdf forms")
				.action(ArgAction::SetTrue)
				.required(false)
		)
		.arg(
			Arg::new("no-escape")
				.short('e')
				.long("no-escape")
				.default_value("false")
				.help("Don't escape special characters")
				.action(ArgAction::SetTrue)
				.required(false)
		)
		.after_help(
			"If no seed is provided for the random number generator then the number of seconds since the \
			 start of the UNIX epoch is used. The default number of variants to generate is 30 and the \
			 default name for the question bank file is \"questions.xml\".\n\
			 Visit https://gitlab.com/vl_garistov/vmks_exam_generator for an explanation of the question \
			 bank file format. An example is provided in example/example_questions.xml."
		);

	// Parse command-line arguments
	process::exit(match cli.try_get_matches()
	{
		// Construct configuration values from command line arguments
		Ok(arg_matches) => match Config::new(arg_matches)
		{
			// Run the main part of the application
			Ok(config) => match vmks_exam_generator::run(config)
			{
				Ok(()) => 0,
				Err(err) =>
				{
					eprintln!("Error: {}", err);
					2
				}
			},
			Err(err) =>
			{
				eprintln!("Invalid arguments: {}", err);
				3
			}
		},
		Err(err) =>
		{
			match err.kind()
			{
				ErrorKind::DisplayHelp | ErrorKind::DisplayVersion =>
				{
					println!("{}", err);
					0
				},
				_ =>
				{
					eprintln!("{}", err);
					1
				}
			}
		}
	});
}
