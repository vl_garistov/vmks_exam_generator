/*
 *	VMKS exam generator - A simple program for pseudo-randomly generating different variants of an embedded programming exam
 *	Copyright (C) 2022 - 2023 Vladimir Garistov <vl.garistov@gmail.com>
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as published
 *	by the Free Software Foundation, version 3.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

mod question;

use std::io::Read;
use std::error::Error;

use rand::seq::SliceRandom;
use rand_pcg::Lcg128Xsl64;
use xml::reader::{EventReader, XmlEvent};

use crate::question_bank::question_group::question::Question;
use crate::constants::*;
use crate::{helpers, VMKSError};

pub struct QuestionGroup
{
	questions: Vec<Question>,
	pick: u32,
	shuffle: bool,
	title: Option<String>,
	description: Option<String>
}

impl QuestionGroup
{
	pub fn from_xml_reader<R: Read>(
		xml_reader: &mut EventReader<R>,
		title: Option<String>,
		questions_to_pick: Option<u32>,
		shuffle: bool
	) -> Result<QuestionGroup, Box<dyn Error>>
	{
		let mut questions: Vec<Question> = Vec::new();
		let mut parsing_description = false;
		let mut description: Option<String> = None;

		loop
		{
			let xml_event = xml_reader.next()?;
			match xml_event
			{
				XmlEvent::Whitespace(_) => continue,
				XmlEvent::StartElement { name, attributes, .. } =>
				{
					//let mut attrib_iter = attributes.iter();
					match &*name.local_name
					{
						DESCRIPTION_TAG_NAME | DESCRIPTION_TAG_NAME_LONG =>
						{
							parsing_description = true;
							helpers::err_if_more_attribs(attributes)?;
						},
						QUESTION_MC_TAG_NAME | QUESTION_MC_TAG_NAME_LONG =>
						{
							let mut shuffle_mc: bool = true;
							for attr in attributes
							{
								if attr.name.local_name == SHUFFLE_ATTRIBUTE_NAME
								{
									shuffle_mc = helpers::get_shuffle_from_attrib(&attr)?;
								}
								else
								{
									return Err(Box::new(VMKSError::UnexpectedAttribute {
										expected: format!("\"{}\" or no more attributes", SHUFFLE_ATTRIBUTE_NAME),
										received: attr.name.local_name.clone()
									}));
								}
							}
							questions.push(Question::multiple_choice_from_xml_reader(xml_reader, shuffle_mc)?)
						},
						QUESTION_VAR_TAG_NAME | QUESTION_VAR_TAG_NAME_LONG =>
						{
							let mut text_field_height = DEFAULT_TEXT_FIELD_HEIGHT;
							for attr in attributes
							{
								if attr.name.local_name == TEXT_FIELD_ATTRIBUTE_NAME_LONG
									|| attr.name.local_name == TEXT_FIELD_ATTRIBUTE_NAME
								{
									text_field_height = attr.value.trim().parse::<u16>()?;
								}
								else
								{
									return Err(Box::new(VMKSError::UnexpectedAttribute {
										expected: format!("\"{}\" or no more attributes", TEXT_FIELD_ATTRIBUTE_NAME_LONG),
										received: attr.name.local_name.clone()
									}));
								}
							}
							questions.push(Question::variable_from_xml_reader(xml_reader, text_field_height)?);
						},
						_ =>
						{
							return Err(Box::new(VMKSError::UnexpectedTag {
								expected: format!(
									"\"{}\", \"{}\" or \"/{}\"",
									QUESTION_MC_TAG_NAME_LONG, QUESTION_VAR_TAG_NAME_LONG, QUESTION_GROUP_TAG_NAME_LONG
								),
								received: name.local_name
							}));
						}
					}
				},
				XmlEvent::Characters(text) =>
				{
					if parsing_description
					{
						description = Some(text.trim().to_string());
					}
					else
					{
						return Err(Box::new(VMKSError::UnexpectedText {
							expected: format!(
								"\"{}\", \"{}\", \"{}\" or \"/{}\"",
								QUESTION_MC_TAG_NAME_LONG, QUESTION_VAR_TAG_NAME_LONG, DESCRIPTION_TAG_NAME_LONG, QUESTION_GROUP_TAG_NAME_LONG
							),
							received: text
						}));
					}
				},
				XmlEvent::EndElement { name } =>
				{
					if parsing_description
					{
						helpers::err_if_not_description(&name)?;
						parsing_description = false;
					}
					else
					{
						if name.local_name != QUESTION_GROUP_TAG_NAME && name.local_name != QUESTION_GROUP_TAG_NAME_LONG
						{
							return Err(Box::new(VMKSError::UnexpectedTag {
								expected: format!(
									"\"{}\", \"{}\", \"{}\" or \"/{}\"",
									QUESTION_MC_TAG_NAME_LONG,
									QUESTION_VAR_TAG_NAME_LONG,
									DESCRIPTION_TAG_NAME_LONG,
									QUESTION_GROUP_TAG_NAME_LONG
								),
								received: name.local_name
							}));
						}
						break;
					}
				},
				_ => return Err(Box::new(VMKSError::GenericParseError))
			}
		}

		let pick = match questions_to_pick
		{
			None => questions.len() as u32,
			Some(n) if n as usize > questions.len() => return Err(Box::new(VMKSError::PickTooMany)),
			Some(n) => n
		};

		Ok(QuestionGroup {
			questions,
			pick,
			shuffle,
			title,
			description
		})
	}

	pub fn randomise_txt(&self, rng: &mut Lcg128Xsl64, question_counter: &mut u32) -> String
	{
		let mut random_selection = String::with_capacity(2048);
		let questions_range = 0..self.questions.len();
		let mut questions_range = questions_range.collect::<Vec<usize>>();

		if let Some(title_text) = &self.title
		{
			random_selection.push_str(title_text);
			if let Some(desc_text) = &self.description
			{
				random_selection.push('\n');
				random_selection.push_str(desc_text);
			}
			random_selection.push_str("\n\n");
		}

		if self.shuffle
		{
			questions_range.shuffle(rng);
		}

		*question_counter += 1;
		random_selection.push_str(&format!(
			"Задача {}: {}",
			*question_counter,
			self.questions[questions_range[0]].randomise_txt(rng)
		));
		for i in 1..self.pick as usize
		{
			*question_counter += 1;
			random_selection.push_str(&format!(
				"\n\nЗадача {}: {}",
				*question_counter,
				self.questions[questions_range[i]].randomise_txt(rng)
			));
		}

		random_selection
	}

	pub fn randomise_pdf(&self, rng: &mut Lcg128Xsl64, question_counter: &mut u32, escape: bool) -> String
	{
		let mut random_selection = String::with_capacity(2048);
		random_selection.push_str("\\noindent\n");
		let questions_range = 0..self.questions.len();
		let mut questions_range = questions_range.collect::<Vec<usize>>();

		if let Some(title_text) = &self.title
		{
			random_selection.push_str(&format!("\\textbf{{{}}}\n\\newline\n", title_text));
		}
		if let Some(desc_text) = &self.description
		{
			random_selection.push_str(&format!("\\indent\n{}\n\\newline\n\\newline\n", desc_text));
		}

		if self.shuffle
		{
			questions_range.shuffle(rng);
		}

		*question_counter += 1;
		random_selection.push_str(&format!(
			"Задача {}: {}",
			*question_counter,
			self.questions[questions_range[0]].randomise_pdf(rng, escape)
		));
		for i in 1..self.pick as usize
		{
			*question_counter += 1;
			random_selection.push_str(&format!(
				"\n\\newline\n\\newline\nЗадача {}: {}",
				*question_counter,
				self.questions[questions_range[i]].randomise_pdf(rng, escape)
			));
		}

		random_selection
	}
}
