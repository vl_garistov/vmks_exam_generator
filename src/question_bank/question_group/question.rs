/*
 *	VMKS exam generator - A simple program for pseudo-randomly generating different variants of an embedded programming exam
 *	Copyright (C) 2022 - 2023 Vladimir Garistov <vl.garistov@gmail.com>
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as published
 *	by the Free Software Foundation, version 3.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::io::Read;
use std::error::Error;

use rand::Rng;
use rand::seq::SliceRandom;
use rand_pcg::Lcg128Xsl64;
use xml::name::OwnedName;
use xml::reader::{EventReader, XmlEvent};

use crate::constants::*;
use crate::{helpers, VMKSError};

pub enum Question
{
	// Multiple Choice
	MC
	{
		question_text: String,
		possible_answers: Vec<String>,
		shuffle: bool
	},
	// Variable text
	Var
	{
		segments: Vec<VarQSegment>,
		text_field_height: u16
	}
}

impl Question
{
	pub fn multiple_choice_from_xml_reader<R: Read>(
		xml_reader: &mut EventReader<R>,
		shuffle: bool
	) -> Result<Question, Box<dyn Error>>
	{
		let mut state = MCQParsingFSM::EndOrQuestionTextOpen;
		let mut question_text = String::new();
		let mut possible_answers: Vec<String> = Vec::new();

		loop
		{
			let xml_event = xml_reader.next()?;
			match xml_event
			{
				XmlEvent::Whitespace(_) => continue,
				XmlEvent::StartElement { name, attributes, .. } =>
				{
					helpers::err_if_more_attribs(attributes)?;
					match &*name.local_name
					{
						QUESTION_TEXT_TAG_NAME | QUESTION_TEXT_TAG_NAME_LONG =>
						{
							state.advance(MCQParsingFSM::EndOrQuestionTextOpen, MCQParsingFSM::QuestionTextBody, &name)?;
						},
						ANSWER_MC_TAG_NAME | ANSWER_MC_TAG_NAME_LONG =>
						{
							state.advance(MCQParsingFSM::EndOrAnswerOpen, MCQParsingFSM::AnswerBody, &name)?;
						},
						_ =>
						{
							return Err(Box::new(VMKSError::UnexpectedTag {
								expected: state.get_expected(),
								received: name.local_name
							}))
						},
					}
				},
				XmlEvent::Characters(text) =>
				{
					let trimmed_text = text.trim().to_string();
					match state
					{
						MCQParsingFSM::QuestionTextBody =>
						{
							question_text = trimmed_text;
							state = MCQParsingFSM::QuestionTextClose;
						},
						MCQParsingFSM::AnswerBody =>
						{
							possible_answers.push(trimmed_text);
							state = MCQParsingFSM::AnswerClose;
						},
						_ =>
						{
							return Err(Box::new(VMKSError::UnexpectedText {
								expected: state.get_expected(),
								received: trimmed_text
							}))
						},
					}
				},
				XmlEvent::EndElement { name } => match &*name.local_name
				{
					QUESTION_TEXT_TAG_NAME | QUESTION_TEXT_TAG_NAME_LONG =>
					{
						state.advance(MCQParsingFSM::QuestionTextClose, MCQParsingFSM::EndOrAnswerOpen, &name)?;
					},
					ANSWER_MC_TAG_NAME | ANSWER_MC_TAG_NAME_LONG =>
					{
						state.advance(MCQParsingFSM::AnswerClose, MCQParsingFSM::EndOrAnswerOpen, &name)?;
					},
					QUESTION_MC_TAG_NAME | QUESTION_MC_TAG_NAME_LONG =>
					{
						if state == MCQParsingFSM::EndOrQuestionTextOpen || state == MCQParsingFSM::EndOrAnswerOpen
						{
							break;
						}
						else
						{
							return Err(Box::new(VMKSError::UnexpectedTag {
								expected: state.get_expected(),
								received: name.local_name
							}));
						}
					},
					_ =>
					{
						return Err(Box::new(VMKSError::UnexpectedTag {
							expected: state.get_expected(),
							received: name.local_name
						}))
					},
				},
				_ => return Err(Box::new(VMKSError::GenericParseError))
			}
		}

		Ok(Question::MC {
			question_text,
			possible_answers,
			shuffle
		})
	}

	pub fn variable_from_xml_reader<R: Read>(
		xml_reader: &mut EventReader<R>,
		text_field_height: u16
	) -> Result<Question, Box<dyn Error>>
	{
		let mut state = VarQParsingFSM::EndOrQuestionTextOpenOrVarTextOpen;
		let mut segments: Vec<VarQSegment> = Vec::new();

		loop
		{
			let xml_event = xml_reader.next()?;
			match xml_event
			{
				XmlEvent::Whitespace(_) => continue,
				XmlEvent::StartElement { name, attributes, .. } =>
				{
					match &*name.local_name
					{
						QUESTION_TEXT_TAG_NAME | QUESTION_TEXT_TAG_NAME_LONG =>
						{
							helpers::err_if_more_attribs(attributes)?;
							state.advance(
								VarQParsingFSM::EndOrQuestionTextOpenOrVarTextOpen,
								VarQParsingFSM::QuestionTextBody,
								&name
							)?;
						},
						VAR_TEXT_TAG_NAME | VAR_TEXT_TAG_NAME_LONG =>
						{
							helpers::err_if_more_attribs(attributes)?;
							state.advance(
								VarQParsingFSM::EndOrQuestionTextOpenOrVarTextOpen,
								VarQParsingFSM::VarTextCloseOrOptionOpenOrVarQuestionOpen,
								&name
							)?;
							segments.push(VarQSegment::VariableText(Vec::new()));
						},
						TEXT_OPTION_TAG_NAME | TEXT_OPTION_TAG_NAME_LONG =>
						{
							helpers::err_if_more_attribs(attributes)?;
							state.advance(
								VarQParsingFSM::VarTextCloseOrOptionOpenOrVarQuestionOpen,
								VarQParsingFSM::OptionBody,
								&name
							)?;
						},
						QUESTION_VAR_TAG_NAME | QUESTION_VAR_TAG_NAME_LONG =>
						{
							if state == VarQParsingFSM::VarTextCloseOrOptionOpenOrVarQuestionOpen
							{
								let mut inner_text_field_height = DEFAULT_TEXT_FIELD_HEIGHT;
								for attr in attributes
								{
									if attr.name.local_name == TEXT_FIELD_ATTRIBUTE_NAME_LONG
										|| attr.name.local_name == TEXT_FIELD_ATTRIBUTE_NAME
									{
										inner_text_field_height = attr.value.trim().parse::<u16>()?;
									}
									else
									{
										return Err(Box::new(VMKSError::UnexpectedAttribute {
											expected: format!("\"{}\" or no more attributes", TEXT_FIELD_ATTRIBUTE_NAME_LONG),
											received: attr.name.local_name.clone()
										}));
									}
								}
								// state intentionally remains VQ_Parsing_FSM::EndOrQuestionTextOpenOrVarTextOpen
								let inner_vq = VarQOption::CompoundOption(Question::variable_from_xml_reader(
									xml_reader,
									inner_text_field_height
								)?);
								let last_segment_index = segments.len() - 1;
								match &mut segments[last_segment_index]
								{
									VarQSegment::VariableText(options) =>
									{
										options.push(inner_vq);
									},
									_ => panic!("attempted to add options to question_text")
								}
							}
							else
							{
								return Err(Box::new(VMKSError::UnexpectedTag {
									expected: state.get_expected(),
									received: name.local_name
								}));
							}
						},
						_ =>
						{
							return Err(Box::new(VMKSError::UnexpectedTag {
								expected: state.get_expected(),
								received: name.local_name
							}));
						}
					}
				},
				XmlEvent::Characters(text) => match state
				{
					VarQParsingFSM::QuestionTextBody =>
					{
						segments.push(VarQSegment::QuestionText(text));
						state = VarQParsingFSM::QuestionTextClose;
					},
					VarQParsingFSM::OptionBody =>
					{
						let last_segment_index = segments.len() - 1;
						match &mut segments[last_segment_index]
						{
							VarQSegment::VariableText(options) =>
							{
								options.push(VarQOption::SimpleOption(text));
							},
							_ => panic!("attempted to add options to question_text")
						};
						state = VarQParsingFSM::OptionClose;
					},
					_ =>
					{
						return Err(Box::new(VMKSError::UnexpectedText {
							expected: state.get_expected(),
							received: text
						}))
					},
				},
				XmlEvent::EndElement { name } => match &*name.local_name
				{
					QUESTION_TEXT_TAG_NAME | QUESTION_TEXT_TAG_NAME_LONG =>
					{
						state.advance(
							VarQParsingFSM::QuestionTextClose,
							VarQParsingFSM::EndOrQuestionTextOpenOrVarTextOpen,
							&name
						)?;
					},
					VAR_TEXT_TAG_NAME | VAR_TEXT_TAG_NAME_LONG =>
					{
						state.advance(
							VarQParsingFSM::VarTextCloseOrOptionOpenOrVarQuestionOpen,
							VarQParsingFSM::EndOrQuestionTextOpenOrVarTextOpen,
							&name
						)?;
					},
					TEXT_OPTION_TAG_NAME | TEXT_OPTION_TAG_NAME_LONG =>
					{
						state.advance(
							VarQParsingFSM::OptionClose,
							VarQParsingFSM::VarTextCloseOrOptionOpenOrVarQuestionOpen,
							&name
						)?;
					},
					QUESTION_VAR_TAG_NAME | QUESTION_VAR_TAG_NAME_LONG =>
					{
						if state == VarQParsingFSM::EndOrQuestionTextOpenOrVarTextOpen
						{
							break;
						}
						else
						{
							return Err(Box::new(VMKSError::UnexpectedTag {
								expected: state.get_expected(),
								received: name.local_name
							}));
						}
					},
					_ =>
					{
						return Err(Box::new(VMKSError::UnexpectedTag {
							expected: state.get_expected(),
							received: name.local_name
						}))
					},
				},
				_ => return Err(Box::new(VMKSError::GenericParseError))
			}
		}

		Ok(Question::Var {
			segments,
			text_field_height
		})
	}

	pub fn randomise_txt(&self, rng: &mut Lcg128Xsl64) -> String
	{
		let mut randomised_question = String::with_capacity(1024);

		match self
		{
			Question::MC {
				question_text,
				possible_answers,
				shuffle
			} =>
			{
				randomised_question.push_str(question_text);
				let num_answers = possible_answers.len();
				let answers_range = 0..num_answers;
				let mut answers_range = answers_range.collect::<Vec<usize>>();
				if *shuffle
				{
					answers_range.shuffle(rng);
				}
				for i in 0..num_answers
				{
					randomised_question.push_str("\n\t- ");
					randomised_question.push_str(&possible_answers[answers_range[i]]);
				}
			},
			Question::Var { segments, .. } =>
			{
				randomise_varq_segments(segments, rng, &mut randomised_question);
			}
		}

		randomised_question
	}

	pub fn randomise_pdf(&self, rng: &mut Lcg128Xsl64, escape: bool) -> String
	{
		let mut randomised_question = String::with_capacity(1024);

		match self
		{
			Question::MC {
				question_text,
				possible_answers,
				shuffle
			} =>
			{
				if escape
				{
					let mut question_text = question_text.clone();
					helpers::replace_specials_latex(&mut question_text);
					randomised_question.push_str(&question_text);
				}
				else
				{
					randomised_question.push_str(question_text);
				}

				let num_answers = possible_answers.len();
				let answers_range = 0..num_answers;
				let mut answers_range = answers_range.collect::<Vec<usize>>();
				if *shuffle
				{
					answers_range.shuffle(rng);
				}
				if escape
				{
					for i in 0..num_answers
					{
						let mut answer = possible_answers[answers_range[i]].clone();
						helpers::replace_specials_latex(&mut answer);
						randomised_question.push_str(&format!("\n\\newline\n\\indent\n\\CheckBox{{{}}}", &answer));
					}
				}
				else
				{
					for i in 0..num_answers
					{
						randomised_question.push_str(&format!(
							"\n\\newline\n\\indent\n\\CheckBox{{{}}}",
							&possible_answers[answers_range[i]]
						));
					}
				}
			},
			Question::Var {
				segments,
				text_field_height
			} =>
			{
				let mut q_text_buf = String::new();
				randomise_varq_segments(segments, rng, &mut q_text_buf);
				if escape
				{
					helpers::replace_specials_latex(&mut q_text_buf);
				}
				if *text_field_height > 0
				{
					q_text_buf.push_str("\\newline\\indent");
					randomised_question.push_str(&format!(
						"\\TextField[multiline=true, height={}\\baselineskip]{{{}}}",
						text_field_height, q_text_buf
					));
				}
				else
				{
					randomised_question.push_str(&q_text_buf);
				}
			}
		}

		randomised_question
	}
}

trait QParseFSM: Sized + PartialEq
{
	fn get_expected(&self) -> String;

	fn advance(&mut self, expected_state: Self, next_state: Self, name: &OwnedName) -> Result<(), Box<dyn Error>>
	{
		if *self == expected_state
		{
			*self = next_state;
			Ok(())
		}
		else
		{
			Err(Box::new(VMKSError::UnexpectedTag {
				expected: self.get_expected(),
				received: name.local_name.clone()
			}))
		}
	}
}

// Multiple choice question parsing finite state machine
#[derive(PartialEq)]
enum MCQParsingFSM
{
	EndOrQuestionTextOpen,
	QuestionTextBody,
	QuestionTextClose,
	EndOrAnswerOpen,
	AnswerBody,
	AnswerClose
}

impl QParseFSM for MCQParsingFSM
{
	fn get_expected(&self) -> String
	{
		match self
		{
			MCQParsingFSM::EndOrQuestionTextOpen =>
			{
				format!("\"{}\" or \"/{}\"", QUESTION_TEXT_TAG_NAME_LONG, QUESTION_MC_TAG_NAME_LONG)
			},
			MCQParsingFSM::QuestionTextBody => "question text".to_string(),
			MCQParsingFSM::QuestionTextClose => format!("\"/{}\"", QUESTION_TEXT_TAG_NAME_LONG),
			MCQParsingFSM::EndOrAnswerOpen => format!("\"{}\" or \"/{}\"", ANSWER_MC_TAG_NAME_LONG, QUESTION_MC_TAG_NAME_LONG),
			MCQParsingFSM::AnswerBody => "answer text".to_string(),
			MCQParsingFSM::AnswerClose => format!("\"/{}\"", ANSWER_MC_TAG_NAME_LONG)
		}
	}
}

// Variable question parsing finite state machine
#[derive(PartialEq)]
enum VarQParsingFSM
{
	EndOrQuestionTextOpenOrVarTextOpen,
	QuestionTextBody,
	QuestionTextClose,
	VarTextCloseOrOptionOpenOrVarQuestionOpen,
	OptionBody,
	OptionClose
}

impl QParseFSM for VarQParsingFSM
{
	fn get_expected(&self) -> String
	{
		match self
		{
			VarQParsingFSM::EndOrQuestionTextOpenOrVarTextOpen => format!(
				"\"{}\", \"{}\" or \"/{}\"",
				QUESTION_TEXT_TAG_NAME_LONG, VAR_TEXT_TAG_NAME_LONG, QUESTION_VAR_TAG_NAME_LONG
			),
			VarQParsingFSM::QuestionTextBody => "question text".to_string(),
			VarQParsingFSM::QuestionTextClose => format!("\"/{}\"", QUESTION_TEXT_TAG_NAME_LONG),
			VarQParsingFSM::VarTextCloseOrOptionOpenOrVarQuestionOpen => format!(
				"\"{}\", \"/{}\" or \"{}\"",
				TEXT_OPTION_TAG_NAME_LONG, VAR_TEXT_TAG_NAME_LONG, QUESTION_VAR_TAG_NAME_LONG
			),
			VarQParsingFSM::OptionBody => "optional text".to_string(),
			VarQParsingFSM::OptionClose => format!("\"/{}\"", TEXT_OPTION_TAG_NAME_LONG)
		}
	}
}

pub enum VarQSegment
{
	QuestionText(String),
	VariableText(Vec<VarQOption>)
}

pub enum VarQOption
{
	SimpleOption(String),
	CompoundOption(Question)
}

impl VarQOption
{
	fn randomise(&self, rng: &mut Lcg128Xsl64) -> String
	{
		match self
		{
			VarQOption::SimpleOption(text) => text.clone(),
			VarQOption::CompoundOption(inner_question) => match inner_question
			{
				Question::Var { .. } => inner_question.randomise_txt(rng),
				Question::MC { .. } => panic!("multiple choice question found inside a variable text")
			}
		}
	}
}

fn randomise_varq_segments(segments: &Vec<VarQSegment>, rng: &mut Lcg128Xsl64, q_text_buf: &mut String)
{
	for segment in segments
	{
		match segment
		{
			VarQSegment::QuestionText(text) => q_text_buf.push_str(text),
			VarQSegment::VariableText(options) => q_text_buf.push_str(&options[rng.gen_range(0..options.len())].randomise(rng))
		}
	}
}
