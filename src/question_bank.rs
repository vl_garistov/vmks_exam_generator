/*
 *	VMKS exam generator - A simple program for pseudo-randomly generating different variants of an embedded programming exam
 *	Copyright (C) 2022 - 2023 Vladimir Garistov <vl.garistov@gmail.com>
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as published
 *	by the Free Software Foundation, version 3.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

mod question_group;

use std::fs::File;
use std::io::Read;
use std::error::Error;
use std::ffi::OsStr;

use rand_pcg::Lcg128Xsl64;
use xml::reader::{EventReader, ParserConfig, XmlEvent};
use latex::{Document, DocumentClass, Element, Paragraph, ParagraphElement, PreambleElement};
use xml::attribute::OwnedAttribute;
use xml::name::OwnedName;

use crate::question_bank::question_group::QuestionGroup;
use crate::constants::*;
use crate::{helpers, VMKSError};

pub struct QuestionBank
{
	question_groups: Vec<QuestionGroup>,
	title: Option<String>,
	description: Option<String>
}

impl QuestionBank
{
	pub fn from_file(filename: &OsStr) -> Result<QuestionBank, Box<dyn Error>>
	{
		let mut qb_file_content = String::with_capacity(8192);

		let mut qb_file = File::open(filename)?;
		qb_file.read_to_string(&mut qb_file_content)?;

		Self::from_str(&qb_file_content)
	}

	pub fn random_variant_txt(&self, rng: &mut Lcg128Xsl64, escape: bool) -> String
	{
		let mut variant = String::with_capacity(4096);
		let mut question_counter: u32 = 0;

		if let Some(title_text) = &self.title
		{
			variant.push_str(title_text);
			variant.push_str("\n\n");
		}
		if let Some(desc_text) = &self.description
		{
			variant.push_str(desc_text);
			variant.push_str("\n\n");
		}

		variant.push_str(&self.question_groups[0].randomise_txt(rng, &mut question_counter));
		for q_group in &self.question_groups[1..]
		{
			variant.push_str("\n\n");
			variant.push_str(&q_group.randomise_txt(rng, &mut question_counter));
		}

		if escape
		{
			helpers::replace_specials_plain(&mut variant);
		}

		// Add a trailing newline
		variant.push('\n');

		variant
	}

	pub fn random_variant_pdf(&self, rng: &mut Lcg128Xsl64, escape: bool) -> Result<String, Box<dyn Error>>
	{
		let mut variant = Document::new(DocumentClass::Article);
		variant.preamble.use_package("hyperref");
		// Preamble::push() is used because Preamble::use_package() does not support arguments.
		variant.preamble.push(PreambleElement::UsePackage {
			package: String::from("inputenc"),
			argument: Some(String::from("utf8"))
		});
		variant.preamble.push(PreambleElement::UsePackage {
			package: String::from("fontenc"),
			argument: Some(String::from("T1,T2A"))
		});
		variant.preamble.push(PreambleElement::UserDefined(String::from(
			"\\renewcommand{\\LayoutCheckField}[2]{#2 #1}"
		)));
		variant.preamble.push(PreambleElement::UserDefined(String::from(
			"\\renewcommand{\\DefaultWidthofText}{0.85\\linewidth}"
		)));
		if let Some(title_text) = &self.title
		{
			if escape
			{
				let mut title_text = title_text.clone();
				helpers::replace_specials_latex(&mut title_text);
				variant.preamble.title(&title_text);
			}
			else
			{
				variant.preamble.title(title_text);
			}
			// Omit the date when rendering title text
			variant.push(Element::UserDefined(String::from("\\date{}")));
			variant.push(Element::TitlePage);
		}
		variant.preamble.author(FILE_AUTHOR);
		if let Some(desc_text) = &self.description
		{
			let mut par = Paragraph::new();
			if escape
			{
				let mut desc_text = desc_text.clone();
				helpers::replace_specials_latex(&mut desc_text);
				par.push(ParagraphElement::Plain(desc_text));
			}
			else
			{
				par.push(ParagraphElement::Plain(desc_text.clone()));
			}
			par.push(ParagraphElement::Plain(String::from("\n\\newline\n\\newline")));
			variant.push(par);
		}

		let mut form_questions: Vec<String> = Vec::new();
		let mut question_counter: u32 = 0;
		form_questions.push(self.question_groups[0].randomise_pdf(rng, &mut question_counter, escape));
		for q_group in &self.question_groups[1..]
		{
			form_questions.push(String::from("\\newline"));
			form_questions.push(q_group.randomise_pdf(rng, &mut question_counter, escape));
		}
		variant.push(Element::Environment(String::from("Form"), form_questions));

		Ok(latex::print(&variant)?)
	}

	// This is a separate function to make testing easier
	fn from_str(input: &str) -> Result<QuestionBank, Box<dyn Error>>
	{
		let parser_config = ParserConfig::new().cdata_to_characters(true).ignore_comments(true);
		let qb_file_content = String::from(input);
		let mut question_groups: Vec<QuestionGroup> = Vec::new();
		let title: Option<String>;
		let mut description: Option<String> = None;
		let mut parsing_description = false;
		let mut question_bank_reader = EventReader::new_with_config(qb_file_content.as_bytes(), parser_config);

		// Verify that the first tag is a start-of-document tag
		let XmlEvent::StartDocument { .. } = question_bank_reader.next()?
		else
		{
			return Err(Box::new(VMKSError::GenericParseError));
		};

		// Find opening question_bank tag
		loop
		{
			let xml_event = question_bank_reader.next()?;
			match xml_event
			{
				XmlEvent::Whitespace(_) => continue,
				XmlEvent::StartElement { name, attributes, .. } =>
				{
					title = Self::qb_parse_start_initial(name, attributes)?;
					break;
				},
				XmlEvent::Characters(text) =>
				{
					return Err(Box::new(VMKSError::UnexpectedText {
						expected: format!("\"{}\"", QUESTION_BANK_TAG_NAME_LONG),
						received: text
					}))
				},
				_ => return Err(Box::new(VMKSError::GenericParseError))
			}
		}

		// Find closing question_bank tag
		// Parse all encountered question groups
		loop
		{
			let xml_event = question_bank_reader.next()?;
			match xml_event
			{
				XmlEvent::Whitespace(_) => continue,
				XmlEvent::StartElement { name, attributes, .. } =>
				{
					if let Some(new_qg) =
						Self::qb_parse_start(&mut parsing_description, &mut question_bank_reader, name, attributes)?
					{
						question_groups.push(new_qg);
					}
				},
				XmlEvent::Characters(text) => description = Self::qb_parse_characters(parsing_description, text)?,
				XmlEvent::EndElement { name } =>
				{
					if Self::qb_parse_end(&mut parsing_description, name)?
					{
						break;
					}
				},
				_ => return Err(Box::new(VMKSError::GenericParseError))
			}
		}

		// Find end of document and make sure there is no junk before it
		loop
		{
			let xml_event = question_bank_reader.next()?;
			match xml_event
			{
				XmlEvent::Whitespace(_) => continue,
				XmlEvent::EndDocument => break,
				XmlEvent::Characters(text) =>
				{
					return Err(Box::new(VMKSError::UnexpectedText {
						expected: "end of document".to_string(),
						received: text
					}));
				},
				_ => return Err(Box::new(VMKSError::GenericParseError))
			}
		}

		Ok(QuestionBank {
			question_groups,
			title,
			description
		})
	}

	// Parse XmlEvent::StartElement encountered before the start of the question bank
	fn qb_parse_start_initial(name: OwnedName, attributes: Vec<OwnedAttribute>) -> Result<Option<String>, Box<dyn Error>>
	{
		if name.local_name == QUESTION_BANK_TAG_NAME || name.local_name == QUESTION_BANK_TAG_NAME_LONG
		{
			let mut title = None;
			for attr in attributes
			{
				if attr.name.local_name == TITLE_ATTRIBUTE_NAME
				{
					title = Some(attr.value.trim().to_string());
				}
				else
				{
					return Err(Box::new(VMKSError::UnexpectedAttribute {
						expected: format!("\"{}\" or no more attributes", TITLE_ATTRIBUTE_NAME),
						received: attr.name.local_name.clone()
					}));
				}
			}
			Ok(title)
		}
		else
		{
			Err(Box::new(VMKSError::UnexpectedTag {
				expected: format!("\"{}\"", QUESTION_BANK_TAG_NAME_LONG),
				received: name.local_name
			}))
		}
	}

	// Parse XmlEvent::StartElement encountered inside of the question bank
	fn qb_parse_start(
		parsing_description: &mut bool,
		question_bank_reader: &mut EventReader<&[u8]>,
		name: OwnedName,
		attributes: Vec<OwnedAttribute>
	) -> Result<Option<QuestionGroup>, Box<dyn Error>>
	{
		let mut group_title: Option<String> = None;
		match &*name.local_name
		{
			QUESTION_GROUP_TAG_NAME | QUESTION_GROUP_TAG_NAME_LONG =>
			{
				let mut questions_to_pick: Option<u32> = None;
				let mut shuffle = true;

				for attr in attributes
				{
					if attr.name.local_name == TITLE_ATTRIBUTE_NAME
					{
						group_title = Some(attr.value.trim().to_string());
					}
					else if attr.name.local_name == PICK_ATTRIBUTE_NAME
					{
						match attr.value.parse::<u32>()
						{
							Ok(pick_val) => questions_to_pick = Some(pick_val),
							_ =>
							{
								return Err(Box::new(VMKSError::InvalidPickValue {
									received: attr.value.clone()
								}));
							}
						}
					}
					else if attr.name.local_name == SHUFFLE_ATTRIBUTE_NAME
					{
						shuffle = helpers::get_shuffle_from_attrib(&attr)?;
					}
					else
					{
						return Err(Box::new(VMKSError::UnexpectedAttribute {
							expected: format!(
								"\"{}\", \"{}\", \"{}\" or no more attributes",
								TITLE_ATTRIBUTE_NAME, PICK_ATTRIBUTE_NAME, SHUFFLE_ATTRIBUTE_NAME
							),
							received: attr.name.local_name.clone()
						}));
					}
				}
				Ok(Some(QuestionGroup::from_xml_reader(
					question_bank_reader,
					group_title,
					questions_to_pick,
					shuffle
				)?))
			},
			DESCRIPTION_TAG_NAME | DESCRIPTION_TAG_NAME_LONG =>
			{
				*parsing_description = true;
				Ok(None)
			},
			_ => Err(Box::new(VMKSError::UnexpectedTag {
				expected: format!(
					"\"{}\", \"{}\" or \"/{}\"",
					QUESTION_GROUP_TAG_NAME_LONG, DESCRIPTION_TAG_NAME_LONG, QUESTION_BANK_TAG_NAME_LONG
				),
				received: name.local_name
			}))
		}
	}

	// Parse XmlEvent::Characters encountered inside of the question bank
	fn qb_parse_characters(parsing_description: bool, text: String) -> Result<Option<String>, Box<dyn Error>>
	{
		if parsing_description
		{
			Ok(Some(text.trim().to_string()))
		}
		else
		{
			Err(Box::new(VMKSError::UnexpectedText {
				expected: format!("\"{}\" or \"/{}\"", QUESTION_GROUP_TAG_NAME_LONG, QUESTION_BANK_TAG_NAME_LONG),
				received: text
			}))
		}
	}

	// Parse XmlEvent::EndElement encountered inside of the question bank
	fn qb_parse_end(parsing_description: &mut bool, name: OwnedName) -> Result<bool, Box<dyn Error>>
	{
		if *parsing_description
		{
			helpers::err_if_not_description(&name)?;
			*parsing_description = false;
			Ok(false)
		}
		else
		{
			if &*name.local_name != QUESTION_BANK_TAG_NAME && &*name.local_name != QUESTION_BANK_TAG_NAME_LONG
			{
				return Err(Box::new(VMKSError::UnexpectedTag {
					expected: format!("\"/{}\"", QUESTION_BANK_TAG_NAME_LONG),
					received: name.local_name
				}));
			}
			Ok(true)
		}
	}
}

#[cfg(test)]
mod tests
{
	use rand::prelude::*;

	use super::*;
	use crate::VMKSError;

	#[test]
	fn multiple_choice_question_txt()
	{
		let mut rng = Lcg128Xsl64::seed_from_u64(69);
		let test_qb = QuestionBank::from_str(
			"<question_bank>\
			    <question_group shuffle=\"false\">\
			        <question_mc shuffle=\"false\">\
			            <question_text>Кое?</question_text>\
			            <answer_mc>Това</answer_mc>\
			            <answer_mc>Онова</answer_mc>\
			            <answer_mc>Другото</answer_mc>\
			            <answer_mc>Което и да е</answer_mc>\
			        </question_mc>\
			    </question_group>\
			</question_bank>"
		)
		.unwrap();

		assert_eq!(
			test_qb.random_variant_txt(&mut rng, true),
			"Задача 1: Кое?\n\
			    \t- Това\n\
			    \t- Онова\n\
			    \t- Другото\n\
			    \t- Което и да е\n"
		);
	}

	#[test]
	fn multiple_choice_question_latex()
	{
		let mut rng = Lcg128Xsl64::seed_from_u64(69);
		let test_qb = QuestionBank::from_str(
			"<question_bank>\
			    <question_group shuffle=\"false\">\
			        <question_mc shuffle=\"false\">\
			            <question_text>Кое?</question_text>\
			            <answer_mc>Това</answer_mc>\
			            <answer_mc>Онова</answer_mc>\
			            <answer_mc>Другото</answer_mc>\
			            <answer_mc>Което и да е</answer_mc>\
			        </question_mc>\
			    </question_group>\
			</question_bank>"
		)
		.unwrap();

		assert_eq!(
			test_qb.random_variant_pdf(&mut rng, true).unwrap(),
			"\\documentclass{article}\n\
			\\usepackage{hyperref}\n\
			\\usepackage[utf8]{inputenc}\n\
			\\usepackage[T1,T2A]{fontenc}\n\
			\\renewcommand{\\LayoutCheckField}[2]{#2 #1}\n\
			\\renewcommand{\\DefaultWidthofText}{0.85\\linewidth}\n\
			\n\
			\\author{ТУЕС към ТУ-София}\n\
			\\begin{document}\n\
			\\begin{Form}\n\
			\\noindent\n\
			Задача 1: Кое?\n\
			\\newline\n\
			\\indent\n\
			\\CheckBox{Това}\n\
			\\newline\n\
			\\indent\n\
			\\CheckBox{Онова}\n\
			\\newline\n\
			\\indent\n\
			\\CheckBox{Другото}\n\
			\\newline\n\
			\\indent\n\
			\\CheckBox{Което и да е}\n\
			\\end{Form}\n\
			\\end{document}\n"
		);
	}

	#[test]
	fn variable_question_txt()
	{
		let mut rng = Lcg128Xsl64::seed_from_u64(69);
		let test_qb = QuestionBank::from_str(
			"<question_bank>\
			    <question_group title=\"Тестова група\" shuffle=\"false\">\
			        <question_var>\
			            <question_text>Хакнете </question_text>\
			            <var_text>\
			                <option>НАСА</option>\
			                <option>ЦРУ</option>\
			                <option>ФБР</option>\
			            </var_text>\
			        </question_var>\
			    </question_group>\
			</question_bank>"
		)
		.unwrap();

		assert_eq!(test_qb.random_variant_txt(&mut rng, true), "Тестова група\n\nЗадача 1: Хакнете НАСА\n");
	}

	#[test]
	fn variable_question_latex()
	{
		let mut rng = Lcg128Xsl64::seed_from_u64(69);
		let test_qb = QuestionBank::from_str(
			"<question_bank>\
			    <question_group title=\"test\" shuffle=\"false\">\
			        <question_var>\
			            <question_text>Хакнете </question_text>\
			            <var_text>\
			                <option>НАСА</option>\
			                <option>ЦРУ</option>\
			                <option>ФБР</option>\
			            </var_text>\
			        </question_var>\
			    </question_group>\
			</question_bank>"
		)
		.unwrap();

		assert_eq!(
			test_qb.random_variant_pdf(&mut rng, true).unwrap(),
			"\\documentclass{article}\n\
			\\usepackage{hyperref}\n\
			\\usepackage[utf8]{inputenc}\n\
			\\usepackage[T1,T2A]{fontenc}\n\
			\\renewcommand{\\LayoutCheckField}[2]{#2 #1}\n\
			\\renewcommand{\\DefaultWidthofText}{0.85\\linewidth}\n\
			\n\
			\\author{ТУЕС към ТУ-София}\n\
			\\begin{document}\n\
			\\begin{Form}\n\
			\\noindent\n\
			\\textbf{test}\n\
			\\newline\n\
			Задача 1: \
			\\TextField[multiline=true, height=5\\baselineskip]{Хакнете НАСА\\newline\\indent}\n\
			\\end{Form}\n\
			\\end{document}\n"
		);
	}

	#[test]
	fn nested_variable_questions()
	{
		let mut rng = Lcg128Xsl64::seed_from_u64(69);
		let test_qb = QuestionBank::from_str(
			"<question_bank>\
			    <question_group shuffle=\"false\">\
			        <question_var>\
			            <question_text>Хакнете </question_text>\
			            <var_text>\
			                <question_var>\
			                    <question_text>MOSSAD използвайки </question_text>\
			                    <var_text>\
			                        <option>HTML</option>\
			                        <option>CSS</option>\
			                    </var_text>\
			                </question_var>\
			                <question_var>\
			                    <question_text>MI5 използвайки </question_text>\
			                    <var_text>\
			                        <option>пръчки</option>\
			                        <option>камъни</option>\
			                    </var_text>\
			                </question_var>\
			            </var_text>\
			        </question_var>\
			    </question_group>\
			</question_bank>"
		)
		.unwrap();

		assert_eq!(
			test_qb.random_variant_txt(&mut rng, true),
			"Задача 1: Хакнете MI5 използвайки пръчки\n"
		);
	}

	#[test]
	fn replacements_txt()
	{
		let mut rng = Lcg128Xsl64::seed_from_u64(69);
		let test_qb = QuestionBank::from_str(
			"<question_bank>\
			    <question_group shuffle=\"false\">\
			        <question_mc shuffle=\"false\">\
			            <question_text>Кое?</question_text>\
			            <answer_mc>Това\\\\</answer_mc>\
			            <answer_mc>\\nОнова</answer_mc>\
			            <answer_mc>\\t\\tДругото</answer_mc>\
			            <answer_mc>Което и да е</answer_mc>\
			        </question_mc>\
			    </question_group>\
			</question_bank>"
		)
		.unwrap();

		assert_eq!(
			test_qb.random_variant_txt(&mut rng, true),
			"Задача 1: Кое?\n\
			    \t- Това\\\n\
			    \t- \nОнова\n\
			    \t- \t\tДругото\n\
			    \t- Което и да е\n"
		);
	}

	#[test]
	fn replacements_latex()
	{
		let mut rng = Lcg128Xsl64::seed_from_u64(69);
		let test_qb = QuestionBank::from_str(
			"<question_bank>\
			    <question_group shuffle=\"false\">\
			        <question_mc shuffle=\"false\">\
			            <question_text>Кое?</question_text>\
			            <answer_mc>Това\\\\</answer_mc>\
			            <answer_mc>\\nОнова</answer_mc>\
			            <answer_mc>\\t\\tДругото</answer_mc>\
			            <answer_mc>Което и да е</answer_mc>\
			        </question_mc>\
			    </question_group>\
			</question_bank>"
		)
		.unwrap();

		assert_eq!(
			test_qb.random_variant_pdf(&mut rng, true).unwrap(),
			"\\documentclass{article}\n\
			\\usepackage{hyperref}\n\
			\\usepackage[utf8]{inputenc}\n\
			\\usepackage[T1,T2A]{fontenc}\n\
			\\renewcommand{\\LayoutCheckField}[2]{#2 #1}\n\
			\\renewcommand{\\DefaultWidthofText}{0.85\\linewidth}\n\
			\n\
			\\author{ТУЕС към ТУ-София}\n\
			\\begin{document}\n\
			\\begin{Form}\n\
			\\noindent\n\
			Задача 1: Кое?\n\
			\\newline\n\
			\\indent\n\
			\\CheckBox{Това\\textbackslash }\n\
			\\newline\n\
			\\indent\n\
			\\CheckBox{\\newline Онова}\n\
			\\newline\n\
			\\indent\n\
			\\CheckBox{\\indent \\indent Другото}\n\
			\\newline\n\
			\\indent\n\
			\\CheckBox{Което и да е}\n\
			\\end{Form}\n\
			\\end{document}\n"
		);
	}

	#[test]
	fn unexpected_tag()
	{
		let test_qb = QuestionBank::from_str(
			"<question_bank>\
			    <question_mc shuffle=\"false\">\
			        <question_text>Кое?</question_text>\
			        <answer_mc>Това</answer_mc>\
			        <answer_mc>Онова</answer_mc>\
			        <answer_mc>Другото</answer_mc>\
			        <answer_mc>Което и да е</answer_mc>\
			    </question_mc>\
			</question_bank>"
		);

		match test_qb
		{
			Err(err) => assert_eq!(format!("{err}"), format!(
				"{}",
				VMKSError::UnexpectedTag
				{
					expected: String::from("\"question_group\", \"description\" or \"/question_bank\""),
					received: String::from("question_mc")
				}
			)),
			Ok(_) => panic!(),
		}
	}

	#[test]
	fn unexpected_attribute()
	{
		let test_qb = QuestionBank::from_str(
			"<question_bank>\
			    <question_group shuffle=\"false\">\
			        <question_mc bs=\"a lot\" shuffle=\"false\">\
			            <question_text>Кое?</question_text>\
			            <answer_mc>Това</answer_mc>\
			            <answer_mc>Онова</answer_mc>\
			            <answer_mc>Другото</answer_mc>\
			            <answer_mc>Което и да е</answer_mc>\
			        </question_mc>\
			    </question_group>\
			</question_bank>"
		);

		match test_qb
		{
			Err(err) => assert_eq!(format!("{err}"), format!(
				"{}",
				VMKSError::UnexpectedAttribute
				{
					expected: String::from("\"shuffle\" or no more attributes"),
					received: String::from("bs")
				}
			)),
			Ok(_) => panic!(),
		}
	}

	#[test]
	fn unexpected_text()
	{
		let test_qb = QuestionBank::from_str(
			"<question_bank>\
			    <question_group shuffle=\"false\">\
			        Пълни глупости\
			        <question_mc shuffle=\"false\">\
			            <question_text>Кое?</question_text>\
			            <answer_mc>Това</answer_mc>\
			            <answer_mc>Онова</answer_mc>\
			            <answer_mc>Другото</answer_mc>\
			            <answer_mc>Което и да е</answer_mc>\
			        </question_mc>\
			    </question_group>\
			</question_bank>"
		);

		match test_qb
		{
			Err(err) => assert_eq!(format!("{err}"), format!(
				"{}",
				VMKSError::UnexpectedText
				{
					expected: String::from("\"question_mc\", \"question_var\", \"description\" or \"/question_group\""),
					received: String::from("Пълни глупости")
				}
			)),
			Ok(_) => panic!(),
		}
	}

	#[test]
	fn pick_too_many()
	{
		let test_qb = QuestionBank::from_str(
			"<question_bank>\
			    <question_group pick=\"200\" shuffle=\"false\">\
			        <question_mc shuffle=\"false\">\
			            <question_text>Кое?</question_text>\
			            <answer_mc>Това</answer_mc>\
			            <answer_mc>Онова</answer_mc>\
			            <answer_mc>Другото</answer_mc>\
			            <answer_mc>Което и да е</answer_mc>\
			        </question_mc>\
			    </question_group>\
			</question_bank>"
		);

		match test_qb
		{
			Err(err) => assert_eq!(format!("{err}"), format!(
				"{}",
				VMKSError::PickTooMany
			)),
			Ok(_) => panic!(),
		}
	}

	#[test]
	fn invalid_pick_value()
	{
		let test_qb = QuestionBank::from_str(
			"<question_bank>\
			    <question_group pick=\"all\" shuffle=\"false\">\
			        <question_mc shuffle=\"false\">\
			            <question_text>Кое?</question_text>\
			            <answer_mc>Това</answer_mc>\
			            <answer_mc>Онова</answer_mc>\
			            <answer_mc>Другото</answer_mc>\
			            <answer_mc>Което и да е</answer_mc>\
			        </question_mc>\
			    </question_group>\
			</question_bank>"
		);

		match test_qb
		{
			Err(err) => assert_eq!(format!("{err}"), format!(
				"{}",
				VMKSError::InvalidPickValue	{ received: String::from("all") }
			)),
			Ok(_) => panic!(),
		}
	}

	#[test]
	fn invalid_shuffle_value()
	{
		let test_qb = QuestionBank::from_str(
			"<question_bank>\
			    <question_group shuffle=\"yeah\">\
			        <question_mc shuffle=\"false\">\
			            <question_text>Кое?</question_text>\
			            <answer_mc>Това</answer_mc>\
			            <answer_mc>Онова</answer_mc>\
			            <answer_mc>Другото</answer_mc>\
			            <answer_mc>Което и да е</answer_mc>\
			        </question_mc>\
			    </question_group>\
			</question_bank>"
		);

		match test_qb
		{
			Err(err) => assert_eq!(format!("{err}"), format!(
				"{}",
				VMKSError::InvalidShuffleValue { received: String::from("yeah") }
			)),
			Ok(_) => panic!(),
		}
	}
}
