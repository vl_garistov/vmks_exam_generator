/*
 *	VMKS exam generator - A simple program for pseudo-randomly generating different variants of an embedded programming exam
 *	Copyright (C) 2022 - 2023 Vladimir Garistov <vl.garistov@gmail.com>
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as published
 *	by the Free Software Foundation, version 3.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::error::Error;

use xml::attribute::OwnedAttribute;
use xml::name::OwnedName;

use crate::constants::*;
use crate::VMKSError;

// Replace "\n" with newline and "\t" with indentation
pub fn replace_specials_plain(s: &mut String)
{
	// Replace \n
	let (double_slash_n_matches, _): (Vec<usize>, Vec<&str>) = s.match_indices("\\\\n").unzip();
	let (slash_n_matches, _): (Vec<usize>, Vec<&str>) = s.match_indices("\\n").unzip();
	let mut prev_index: usize = 0;
	let mut offset_index: usize;
	let mut buffer = String::with_capacity(s.len() * 2);

	for index in slash_n_matches.iter()
	{
		buffer.push_str(&s[prev_index..*index]);
		offset_index = index - 1;
		if double_slash_n_matches.contains(&offset_index)
		{
			prev_index = index + 1;
		}
		else
		{
			buffer.push('\n');
			prev_index = index + 2;
		}
	}
	if prev_index < s.len()
	{
		buffer.push_str(&s[prev_index..]);
	}

	s.clear();

	// Replace \t
	let (double_slash_t_matches, _): (Vec<usize>, Vec<&str>) = buffer.match_indices("\\\\t").unzip();
	let (slash_t_matches, _): (Vec<usize>, Vec<&str>) = buffer.match_indices("\\t").unzip();
	prev_index = 0;

	for index in slash_t_matches.iter()
	{
		s.push_str(&buffer[prev_index..*index]);
		offset_index = index - 1;
		if double_slash_t_matches.contains(&offset_index)
		{
			prev_index = index + 1;
		}
		else
		{
			s.push('\t');
			prev_index = index + 2;
		}
	}
	if prev_index < buffer.len()
	{
		s.push_str(&buffer[prev_index..]);
	}

	// Replace any leftover \\
	*s = s.replace("\\\\", "\\");
}

// Escape special LaTeX characters and replace "\n" with newline and "\t" with indentation
pub fn replace_specials_latex(s: &mut String)
{
	let mut s_escaped = String::with_capacity(s.len() + 100);
	let mut escape_next = false;
	for c in s.chars()
	{
		if escape_next
		{
			match c
			{
				'n' => s_escaped.push_str("\\newline "),
				't' => s_escaped.push_str("\\indent "),
				'\\' => s_escaped.push_str("\\textbackslash "),
				_ =>
				{
					s_escaped.push_str("\\textbackslash ");
					s_escaped.push(c);
				}
			}
			escape_next = false;
		}
		else
		{
			match c
			{
				'$' | '%' | '#' | '_' | '&' | '{' | '}' =>
				{
					s_escaped.push('\\');
					s_escaped.push(c);
				},
				'\\' => escape_next = true,
				_ => s_escaped.push(c)
			}
		}
	}
	*s = s_escaped;
}

// Throw an error if there are any more attributes specified
pub fn err_if_more_attribs(attributes: Vec<OwnedAttribute>) -> Result<(), Box<dyn Error>>
{
	if let Some(attr) = attributes.first()
	{
		return Err(Box::new(VMKSError::UnexpectedAttribute {
			expected: "".to_string(),
			received: attr.name.local_name.clone()
		}));
	};
	Ok(())
}

// Throw an error if the name of the XML element is not "description"
pub fn err_if_not_description(name: &OwnedName) -> Result<(), Box<dyn Error>>
{
	if name.local_name == DESCRIPTION_TAG_NAME || name.local_name == DESCRIPTION_TAG_NAME_LONG
	{
		Ok(())
	}
	else
	{
		Err(Box::new(VMKSError::UnexpectedTag {
			expected: format!("\"/{}\"", DESCRIPTION_TAG_NAME_LONG),
			received: name.local_name.clone()
		}))
	}
}

// Parse the value of the shuffle attribute
pub fn get_shuffle_from_attrib(attr: &OwnedAttribute) -> Result<bool, Box<dyn Error>>
{
	match attr.value.parse::<bool>()
	{
		Ok(shuffle_val) => Ok(shuffle_val),
		_ => Err(Box::new(VMKSError::InvalidShuffleValue {
			received: attr.value.clone()
		}))
	}
}

#[cfg(test)]
mod tests
{
	use super::*;

	#[test]
	fn escape_plain()
	{
		// Note that str::replace() gets confused by overlapping matches or at least so it seems
		let mut s = String::from("Lorem Ipsum\\n\\trandom bs \\\\ some \\\\\n more \\t stuff\\\\nblah\\\\t");
		replace_specials_plain(&mut s);
		assert_eq!(s, "Lorem Ipsum\n\trandom bs \\ some \\\n more \t stuff\\nblah\\t");
	}

	#[test]
	fn escape_latex()
	{
		let mut s = String::from(
			"Lorem Ipsum\\n\\trandom bs \\\\ some \\\\\\n more \\t stuff\\\\nblah\\\\t\
			$smth #_ ipsum% {LOREM} &more stuff"
		);
		replace_specials_latex(&mut s);
		assert_eq!(
			s,
			"Lorem Ipsum\\newline \\indent random bs \\textbackslash  some \\textbackslash \\newline  more \
			\\indent  stuff\\textbackslash nblah\\textbackslash t\\$smth \\#\\_ ipsum\\% \\{LOREM\\} \\&more stuff"
		);
	}
}
